package test

import (
	"io/ioutil"
	"log"
	"testing"

	"github.com/go-playground/assert/v2"
	signverifyrsa "gitlab.com/juldaus/sign-verify-rsa"
)

func TestSigning(t *testing.T) {
	priv, err := ioutil.ReadFile("private.pem")
	if err != nil {
		log.Fatal(err)
	}

	signature, err := signverifyrsa.Signing("message to signing", priv)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(signature)
}

func TestVerifying(t *testing.T) {
	base64Signature := "jyyqunZHZoHJV3/QFw05lIcObDtulrbM5fhKmMq5E0IMgHks+inz2b/qcG/4HmJAChp79ucONBZnkDl3H7ikp0Bae3nZIQuY8k94G31NX1oYJ71OZYvsLxGYrlRtj7b/n2uuB3pzz2uFNYQJtjFo+dMEEmeCKE9dkjRJUyeCe27EY7MlYjIbL2LI10/ctjCC2JVgmLaf2W0KAT99rlkG5IyXU+R4UgJ9EEecYR4paA9S64hS/L27WAPp2+Ab0MkC8UAutzZbmmLskl49GROkT33cT5yTa9o1cgd/0mfBwFK7fp6kcWG2TUn3jwjOQHcbQ9Zjc/V0nE6PavA+KEOmPw=="

	pub, err := ioutil.ReadFile("public.pem")
	if err != nil {
		log.Fatal(err)
	}

	verified, err := signverifyrsa.Verifying(base64Signature, "message to signing", pub)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, verified, true)
}

func TestFailedToVerify(t *testing.T) {
	base64Signature := "jyyqunZHZoHJV3/QFw05lIcObDtulrbM5fhKmMq5E0IMgHks+inz2b/qcG/4HmJAChp79ucONBZnkDl3H7ikp0Bae3nZIQuY8k94G31NX1oYJ71OZYvsLxGYrlRtj7b/n2uuB3pzz2uFNYQJtjFo+dMEEmeCKE9dkjRJUyeCe27EY7MlYjIbL2LI10/ctjCC2JVgmLaf2W0KAT99rlkG5IyXU+R4UgJ9EEecYR4paA9S64hS/L27WAPp2+Ab0MkC8UAutzZbmmLskl49GROkT33cT5yTa9o1cgd/0mfBwFK7fp6kcWG2TUn3jwjOQHcbQ9Zjc/V0nE6PavA+KEOmPw=="

	pub, err := ioutil.ReadFile("private.pem")
	if err != nil {
		log.Fatal(err)
	}

	verified, err := signverifyrsa.Verifying(base64Signature, "message to signing", pub)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, verified, false)
}
