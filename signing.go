package signverifyrsa

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
)

func Signing(payload interface{}, private []byte) (string, error) {
	bytePayload, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}
	hashedPayload := sha256.Sum256(bytePayload)

	block, _ := pem.Decode(private)
	key, _ := x509.ParsePKCS1PrivateKey(block.Bytes)

	signature, err := rsa.SignPKCS1v15(rand.Reader, key, crypto.SHA256, hashedPayload[:])
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(signature), nil
}
