package signverifyrsa

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
)

func Verifying(base64Signature string, msg interface{}, public []byte) (bool, error) {
	byteMsg, _ := json.Marshal(msg)

	msgHash := sha256.New()
	if _, err := msgHash.Write([]byte(byteMsg)); err != nil {
		return false, err
	}

	msgHashSum := msgHash.Sum(nil)
	signature, _ := base64.StdEncoding.DecodeString(base64Signature)

	pubPem, _ := pem.Decode(public)
	parsedKey, _ := x509.ParsePKIXPublicKey(pubPem.Bytes)
	pubKey, ok := parsedKey.(*rsa.PublicKey)
	if !ok {
		return false, fmt.Errorf("got unexpected key type: %T", pubKey)
	}

	err := rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, msgHashSum, signature)
	if err != nil {
		return false, err
	}
	return true, nil
}
